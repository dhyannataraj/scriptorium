#!/usr/bin/perl

# This script gets German verb conjugation from wiktionary and saves it to
# format that is sutable for anki program

use strict;

my $word = 'sein';
#my $word = 'sehen';

my $ru =
 {
          'ich' => 'я',
          'du' => 'ты',
          'er' => 'он',
          'wir' => 'мы',
          'ihr' => 'вы',
          'sie' => 'они',
          'sie(f)' => 'она',
          'es' => 'оно',
          'Sie' => 'Вы',
        };


my $page = `curl https://en.wiktionary.org/wiki/$word 2>/dev/null`;
my $res = {};
my $ogg = {};

$page=~s{.*<h2>.*?>German</span>.*?</h2>}{}s or die "German section not found";

$page=~s{<h2>.*?$}{}s;

$page=~s{.*<div class="NavHead" style="">[Cc]onjugation of}{}s or die "Conjugation box not found";

$page=~s{.*>present</th>}{}s  or die "'Present' table header not found";

$page=~s{.*?<td>(ich.*?)</td>}{}s or die "'ich' not found";
$res->{ich}=$1;

$page=~s{.*?<td>(wir.*?)</td>}{}s or die "'wir' not found";
$res->{wir}=$1;

$page=~s{.*?<tr>}{}s or die "tr not found";

$page=~s{.*?<td>(du.*?)</td>}{}s or die "'du' not found";
$res->{du}=$1;

$page=~s{.*?<td>(ihr.*?)</td>}{}s or die "'ihr' not found";
$res->{ihr}=$1;

$page=~s{.*?<tr>}{}s or die "tr not found";

$page=~s{.*?<td>(er.*?)</td>}{}s or die "'er' not found";
$res->{er}=$1;

$page=~s{.*?<td>(sie.*?)</td>}{}s or die "'sie' not found";
$res->{sie}=$1;

foreach my $s (keys %$res)
{
  $res->{$s}=~s{^\w+\s+}{};
  $res->{$s}=~s{^<span.*?>}{};
  $res->{$s}=~s{^<a.*?>}{};
  $res->{$s}=~s{^<strong.*?>}{};
  $res->{$s}=~s{</span>$}{};
  $res->{$s}=~s{</a>$}{};
  $res->{$s}=~s{</strong>$}{};

}

foreach my $pronoun ( 'ich','du', 'er', 'wir', 'ihr','sie')
{
  $ogg->{$pronoun} = get_ogg($res->{$pronoun});
}

$ogg->{'sie(f)'}= $ogg->{er};
$res->{'sie(f)'}= $res->{er};


$ogg->{'es'}= $ogg->{er};
$res->{'es'}= $res->{er};

$ogg->{'Sie'}= $ogg->{sie};
$res->{'Sie'}= $res->{sie};


foreach my $pronoun ( 'ich','du', 'er', 'sie(f)', 'es', 'wir', 'ihr','sie', 'Sie')
{
  my $pronoun_printable = $pronoun;
  $pronoun_printable = 'sie' if $pronoun eq 'sie(f)';
  print "$word → ", $ru->{$pronoun}, "\t$pronoun_printable ".$res->{$pronoun};
  print " [sound:".$ogg->{$pronoun}."]" if $ogg->{$pronoun};
  print "\n";
}



sub get_ogg
{
  my $word = shift;
  my $page = `curl https://de.wiktionary.org/wiki/$word  2>/dev/null`;
  $page=~s{.*?<h2>.*?>Deutsch<.*?</h2>}{}s or die "Deutsch section not found for $word";
  $page=~s{<h2>.*?$}{}s;
  
  $page=~m{href="(//upload.wikimedia.org/wikipedia/commons/.*?.ogg)"} or return undef;
  
  my $file_url = "https:$1";
  $file_url =~ m{/([^/]*?)$} or die;
  my $file = $1;
  die unless $file;
  `rm $file` if (-e $file);
  `wget -q $file_url`;
  return $file;
}

