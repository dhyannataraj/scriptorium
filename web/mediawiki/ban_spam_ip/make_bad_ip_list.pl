#!/usr/bin/perl

use strict;
use experimental 'smartmatch';

# http://www.mediawiki.org/wiki/Manual:Combating_spam#IP_address_blacklists

my $cache_dir = '/srv/bots/cache';
my $config_file =  '/srv/conf/nginx/bad_ip_list.conf';

my $log = "";
if ('daily' ~~ \@ARGV || ! -e "$cache_dir/bannedips.zip")
{
#  print "Ежедневный запуск\n";

  $log.=`rm -f $cache_dir/listed_ip_365.zip`;
  $log.=`cd $cache_dir ; wget http://www.stopforumspam.com/downloads/listed_ip_365.zip 2>&1`;

  my_die("listed_ip_365.zip failed to download") unless -e "$cache_dir/listed_ip_365.zip";

  $log.=`rm -f $cache_dir/listed_ip_365.txt`;
  $log.=`cd $cache_dir ; unzip listed_ip_365.zip`;
  my_die("listed_ip_365.zip failed to extract") unless -e "$cache_dir/listed_ip_365.txt";
}

if (! ('daily' ~~ \@ARGV) || ! -e "$cache_dir/listed_ip_1.zip")
{
#  print "Ежечасный запуск\n";
  $log.=`rm -f $cache_dir/listed_ip_1.zip`;
  $log.=`cd $cache_dir ; wget http://www.stopforumspam.com/downloads/listed_ip_1.zip 2>&1`;
  my_die("listed_ip_1.zip failed to download") unless -e "$cache_dir/listed_ip_1.zip";
  $log.=`rm -f $cache_dir/listed_ip_1.txt`;
  $log.=`cd $cache_dir ; unzip listed_ip_1.zip`;
  my_die("listed_ip_1.zip failed to extract") unless -e "$cache_dir/listed_ip_1.txt";
}

open F2 ,">", "$cache_dir/config_file.new" or   die "cannot open: $!";

print F2 "geo \$bad_ip {\n";
print F2 "    default 0;\n";


my %known_addresses = ();
open F, "$cache_dir/listed_ip_1.txt" or die;
  while (my $ip = <F> )
  {
    $ip =~ s/\n\z// or die;
    my_die("$ip is not IP address") unless is_ip($ip);
    print F2 "    $ip 1;\n";
    $known_addresses{$ip} = 1;
  }
close F;

open F, "$cache_dir/listed_ip_365.txt" or die "Error opening 'listed_ip_365.txt': \$!";
{
#  local $/ = ',';
  while (my $ip = <F> )
  {
    $ip =~ s/\n\z// or die;
    my_die("$ip is not IP address") unless is_ip($ip);
    print F2 "    $ip 1;\n" unless $known_addresses{$ip};
  }
}
close F;

print F2 "}\n";
close F2;

`mv  $config_file $config_file.old`;
`mv $cache_dir/config_file.new $config_file`;
`rm $config_file.old`;

sub is_ip
{
  my $ip = shift;
  $ip =~ /^(\d)+\.(\d+)\.(\d+)\.(\d+)\z/ or return 0;
  return 0 if $1<0 || $1>255;
  return 0 if $2<0 || $2>255;
  return 0 if $3<0 || $3>255;
  return 0 if $4<0 || $4>255;
  return 1;
}

sub my_die
{
  print $log;
  die @_;
}