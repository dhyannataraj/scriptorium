# make_bad_ip_list.pl

Этот скрипт получает из сервиса www.stopforumspam.com список IP адресов
замеченных в распростронении спама, после чего формотирует его как
ассоциотивный массив сопоставляющий ip-адресу числовое значение:

```
geo $bad_ip {
    default 0;
    1.0.133.100 1;
    1.0.254.92 1;
....
    232.130.64.182 1;
    237.233.162.122 1;
    239.165.161.122 1;
    239.177.56.27 1;
}
```

Далее файл с этим массивом inlude'иться в основной конфиг, и в конфигах
обслуживаемых сайтов можно проводить проверку попадания клиентского IP в
список опорочивших себя адресов и предпринять какие-либо действия в связи
с этим.

```
        if ($bad_ip)
        {
          rewrite ^(.*)$ http://ro.wiki.example.com$1;
        }
```
В моем случае происходит перенаправление на read-only версию сайта.

Скрипт работает в двух режимах. Первый режим daily, скачивает полную базу
ip-адресов с сервиса stopforumspam. stopforumspam поpзволяет это делать не
более двух раз в день. Второй режим, hourly, скачивает ежечасные обновления
списка ip-адресов, и добавляет их к основному пулу адресов. Обновления 
скачиваются каждый час.

Пример записей в `/etc/crontab`, выполняющих запуск этого скрипта

```
[минта]  */1	* * *  root /srv/bots/make_bad_ip_list.pl hourly; /etc/init.d/nginx restart  >/dev/null
15 [час1],[час2]	* * *  root /srv/bots/make_bad_ip_list.pl daily; /etc/init.d/nginx restart  >/dev/null
```

Значения `[минута]`, и `[час1]` и `[час2]` подставьте на ваш вкус.