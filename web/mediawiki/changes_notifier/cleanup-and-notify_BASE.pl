#!/usr/bin/perl

use strict;

die unless defined $db_host;
die unless defined $db_name;
die unless defined $db_user;
die unless defined $db_password ;

die unless defined $cache_dir; # Директория для храняния всякой нужной фигни

die unless defined $prefix;  # префикс имен таблиц MediaWiki. Часто 'mw_'
die unless defined $www_host;  # www host по которому доступна wiki из интернета (для формирования ссылок на удаление)



my $timestamp_file = "$cache_dir/$db_name.db_timestamp";

my $name_spaces = {
                    0 => "",
                    2 => "User:",
                    3 => "User_Talk:",
                  };

# удаляем всех пользователей у которых не было ни одной правки
#do_query("delete from ${prefix}user where user_name not in (select distinct rev_user_text from ${prefix}revision );");
do_query("DELETE FROM ${prefix}user WHERE user_name NOT IN (SELECT DISTINCT actor_name FROM ${prefix}revision_actor_temp LEFT JOIN ${prefix}actor ON revactor_actor = actor_id)");
# FIMXE со временем revactor_actor должен куда-то переехать из mw_revision_actor_temp. Запрос тогда надо будет переписать...


# удаляем все упоминания об удаленных пользователях из recent changes
do_query("delete from ${prefix}recentchanges where rc_log_type = \"newusers\" and rc_log_action=\"create\" and rc_title not in (select user_name from ${prefix}user)");

my $present_timestamp = do_query("select max(rc_timestamp) from ${prefix}recentchanges;");

$present_timestamp =~ s/^.*?\n//s;
chomp $present_timestamp;


my $last_timestamp = undef;
if (-e $timestamp_file)
{
  $last_timestamp = `cat $timestamp_file`;
  chomp $last_timestamp;
  $last_timestamp = undef unless $last_timestamp =~ /^\d*$/;
}

$last_timestamp = $present_timestamp - 60*60*24+300 unless $last_timestamp;

# Собираем данные о правках сулчившихся за последние сутки
my $changes = do_query("SELECT actor_name, rc_title, rc_namespace, rc_log_type, rc_log_action FROM ${prefix}recentchanges LEFT JOIN ${prefix}actor ON rc_actor = actor_id WHERE rc_timestamp > $last_timestamp AND (rc_log_type NOT IN (\"delete\", \"block\", \"newusers\") OR rc_log_type is NULL) ");

if ($changes)
{
  print "New changes at $www_host  were made this day:\n\n";
  $changes=~s/^.*?\n//s; # убираем заголовочную строку из полученного вывода
  foreach (split "\n",$changes)
  {
    my($actor, $rc_title,$rc_namespace,$rc_log_type,$rc_log_action) = split("\t",$_);
    my $namespace = $name_spaces->{$rc_namespace} // "?????? Namespace N $rc_namespace:";
    print "$actor\tdid\t$rc_log_type\t $rc_log_action on page\t$namespace$rc_title:\t$www_host/index.php?action=delete&title=$namespace$rc_title\n";
  }
  `echo $present_timestamp > $timestamp_file `;
}

sub do_query
{
  my $query = shift;

  return `mysql $db_name -h $db_host -u $db_user -p$db_password -e '$query';`
}
