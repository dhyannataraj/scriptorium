#!/usr/bin/perl

use strict;
use Path::Tiny;

my $db_host = 'localhost';
my $db_name = 'example_com';
my $db_user = 'u_example_com';
my $db_password = 'my_cool_password';

my $cache_dir = '/srv/bots/cache'; # Директория для храняния всякой нужной фигни

my $prefix = 'mw_';  # префикс имен таблиц MediaWiki. Часто 'mw_'
my $www_host = "http://example.com";  # www host по которому доступна wiki из интернета (для формирования ссылок на удаление)

my $script_dir = path($0)->parent->stringify;

my $code = path($script_dir)->child('cleanup-and-notify_BASE.pl')->slurp;

eval $code;
die $@ if $@;
