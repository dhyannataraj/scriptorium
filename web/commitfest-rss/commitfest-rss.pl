#!/usr/bin/perl

use strict;
use LWP::UserAgent;
use XML::RSS;
use DateTime;
use DateTime::Format::Mail;
use DateTime::Format::Pg;

my $cache_dir = "cache";

my $ua = LWP::UserAgent->new;

my $response = $ua->get('https://commitfest.postgresql.org/');
die $response->status_line unless $response->is_success;
my $top_page = $response->decoded_content;

my $items = [];

while ($top_page=~s{<a href="(/\d+/)">\d\d\d\d-\d\d</a> \(Open}{})
{
  my $cf_link = $1;
  my $response = $ua->get("https://commitfest.postgresql.org$cf_link");
  die $response->status_line unless $response->is_success;
  
  my $cf_page = $response->decoded_content;

  my $topic_name = undef;
  $cf_page =~ s{^.*?<h3>.*?<tr>}{}s;
  foreach my $item_text (split /<tr>/, $cf_page)
  {
    if ($item_text =~ m{<th colspan="9">(.*?)</th>})
    {
      $topic_name = $1;
    }
    next if $item_text =~ /<th[ >]/;

    my $item = {};
#print $item_text;
    $item_text =~ s{<td><a href="(\d+)/">(.*?)</a></td>}{}s or die;
    $item->{id} = "$cf_link$1";
    $item->{title} = $2;
    $item_text =~ s{<td>.*?</td>}{}s or die; # Status
    $item_text =~ s{<td>.*?</td>}{}s or die; # Ver
    $item_text =~ s{<td>(.*?)</td>}{}s or die;
    $item->{author} = $1;
    $item->{topic} = $topic_name;
    die unless defined $topic_name;

   update_item_date($item);
   push @$items, $item;
  }
}

# Now, as we've parsed it, let's build an RSS


my $rss = XML::RSS->new (version => '2.0');
$rss->channel(title          => 'Postresql Commitfest: New patches',
               link           => 'https://commitfest.postgresql.org/',
               language       => 'en',
               description    => 'This feed streams a list of new patches on commitfest.postgresql.org',
#               pubDate        => 'Thu, 23 Aug 1999 07:00:00 GMT',
#               lastBuildDate  => 'Thu, 23 Aug 1999 16:20:26 GMT',
#               managingEditor => 'dhyan@nataraj.su',
               webMaster      => 'dhyan@nataraj.su'
               );

foreach my $item (sort({$b->{created} cmp $a->{created}}  @$items))
{
   $rss->add_item(title      => $item->{title},
                  permaLink  => "https://commitfest.postgresql.org".$item->{id},
                  pubDate    => date_to_RFC822($item->{created}),
                  description => '<b>Title: </b>'.$item->{title}."<br>\n".
                                 "<b>Topic:</b>: ".$item->{topic}."<br>\n".
                                 "<b>Created:</b> ".$item->{created}."<br>\n".
                                 "<b>Author:</b> ".$item->{author}."<br>\n".
                                 "<b>Url:</b> ".make_a("https://commitfest.postgresql.org".$item->{id})."<br>\n"
                                 ,
                );
}


print "Content-Type: application/rss+xml\n\n";
print $rss->as_string;


sub update_item_date
{
  my $item = shift;

  my $page = fetch_or_cache($item->{id});

  $page =~ s{^.*<th>Created</th>}{}s or die;
  $page =~ m{<td>(.*?)</td>} or die ;
  $item->{created} = $1;
  
}

sub fetch_or_cache
{
  my $id = shift;
  
  $id=~s{[^a-zA-Z0-9/]}{?}g; # Secure id for file opening

  my $cache_name = $id;
  $cache_name =~ s{/}{_}g;
  $cache_name = "$cache_dir/$cache_name";
  my $page;

  if ( -e $cache_name)
  {
    $page = slurp($cache_name);
  } else
  {
    my $response = $ua->get("https://commitfest.postgresql.org".$id);
    die $response->status_line unless $response->is_success;

    $page = $response->decoded_content;

    # put it in the cache
    open F, ">", "$cache_name" or die "Error opening file '$cache_name': $!";
    print F $page;
    close F;
  }
  return $page;
}



sub slurp
{
  my $file_name = shift;
  my $s = "";
  open F, $file_name;
  while (my $ss = <F>)
  {
    $s.=$ss;
  }
  return $s;
}

sub date_to_RFC822
{
  my $date = shift;
  my $dt = DateTime::Format::Pg->parse_datetime($date);
  return DateTime::Format::Mail->format_datetime( $dt );
}
sub make_a
{
  my $url = shift;
  return "<a href='$url'>$url</a>";
}
