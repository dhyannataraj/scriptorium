#!/usr/bin/perl

use strict;
use sort 'stable';
use File::Copy;

my $monthly_limit = -1; # Установите в положительное значение чтобы ограничить колличество хранимых ежемесячных бэкапов.
my $base_dir = '/var/backups/[PROJECT_NAME]';
my $in_dir = "$base_dir/incoming";

my $force_weekly_backup = 0;
my $force_monthly_backup = 0;

# Для случая, если ловим отельные дампы
#=cut
my $file_name_base='[PROJECT_NAME]_';
my $file_ext='.sql.gz';
my $store_dir = $base_dir;
#=cut

# ### Для случая если ловим контейнеры
=cut
my $container_name = $ARGV[0];

die "Пожалуйста укажите имя контейнера в качестве первого параметра" unless $container_name;

my $file_name_base="${container_name}_";
my $file_ext='.tgz';

my $store_dir = "$base_dir/$container_name";
=cut


my $date = `date +%F`;
chomp $date;

my $file = "$file_name_base$date$file_ext";

if (! -e "$in_dir/$file")
{
  die "backup data is outdated (file $in_dir/$file do not exist)";
}

if (! -d "$store_dir")
{
  die "backup storage dir $store_dir not found. Please create it manually";
}

my $day_of_week = `date +%u`;
chomp $date;
my $day_of_month = `date +%d`;
chomp $day_of_month;

$day_of_week=7                  if $force_weekly_backup;
print "Weekly backup forsed\n"  if $force_weekly_backup;
$day_of_month=15                if $force_monthly_backup;
print "Monthly backup forsed\n" if $force_monthly_backup;

if ($day_of_week == 7) # В воскресенье делаем еженедельный бэкап
{
  if (! -d "$store_dir/weekly")
  {
    mkdir ("$store_dir/weekly") or die "Error creating dir $store_dir/weekly :$!";
  }
  my $from = "$in_dir/$file";
  my $to = "$store_dir/weekly/$file";
  copy($from,$to) or die "Error copying $from $to";
}

if ($day_of_month == 15) # В середине месяца -- ежемесячный бэкап
{
  if (! -d "$store_dir/monthly")
  {
    mkdir ("$store_dir/monthly") or die "Error creating dir $store_dir/monthly :$!";
  }
  my $from = "$in_dir/$file";
  my $to = "$store_dir/monthly/$file";
  copy($from,$to) or die "Error copying $from $to";
}

# daily
{
  if (! -d "$store_dir/daily")
  {
    mkdir ("$store_dir/daily") or die "Error creating dir $store_dir/daily :$!";
  }

  my $from = "$in_dir/$file";
  my $to = "$store_dir/daily/$file";
  move($from,$to) or die "Error moving file $from to $to";
}

# А теперь убираем все лишнее...

delete_old_backups("$store_dir/daily",7);
delete_old_backups("$store_dir/weekly",4);
delete_old_backups("$store_dir/monthly",$monthly_limit) if $monthly_limit>0;


sub delete_old_backups
{
  my $dir = shift;
  my $limit = shift;

  my @files=();
  opendir(DIR, $dir);
  while (my $file=readdir(DIR))
  {
    if ($file =~ /^\Q$file_name_base\E\d\d\d\d-\d\d-\d\d\Q$file_ext\E$/)
    {
      push @files, $file;
    }
  }
  @files = sort @files;
  for (my $i=0; $i<$limit; $i++)
  {
    delete $files[-1]; # $limit раз отбрасываем последний элемент списка.
  }
  foreach (@files)
  {
    unlink ("$dir/$_") or die "Error deleting file $dir/$_" ;
  }
}