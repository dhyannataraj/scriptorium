#!/bin/bash -e

export WORK_DIR=/.scripts/work
export CONTAINER_NAME=$1;
export CONTAINER_PATH="/.lxc/$CONTAINER_NAME"

export DATE=`date +%F`;
export ARCH_NAME="$WORK_DIR/${CONTAINER_NAME}_$DATE.tgz"
export LOCK_FILE="$WORK_DIR/$CONTAINER_NAME.lock"


# export BACKUP_HOST=[IP]
# export BACKUP_USER=[uresrname]
export BACKUP_DIR=in-dir


if [ ! $CONTAINER_NAME ];
then
    echo "Please specify container name as a first argument"
    exit 1;
fi

if [ ! -d "$CONTAINER_PATH" ];
then
    echo "Container '$CONTAINER' not found"
    exit 1;
fi

lockfile-check $LOCK_FILE || exit_code=$?

if [ ! $exit_code ]
then
    echo "backup script already running. Exitng";
    exit 1;
fi

lockfile-create $LOCK_FILE
lockfile-touch $LOCK_FILE &

export LOKER_PID="$!"

if [ -f $ARCH_NAME ];
then
    echo "File '$ARCH_NAME' for some reason already exists. Afraid to continue working"
    exit 1;
fi

cd $CONTAINER_PATH;  tar -cvzf $ARCH_NAME ./ >/dev/null 2> >(grep -v "socket ignored" >&2)

scp -q $ARCH_NAME $BACKUP_USER@$BACKUP_HOST:$BACKUP_DIR

rm $ARCH_NAME

kill "${LOKER_PID}"
lockfile-remove $LOCK_FILE
